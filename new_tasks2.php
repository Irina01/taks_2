<?php
//createDeepArrayOfNumbers([], 2, random_int(6, 8));
function createDeepArrayOfNumbers($array, int $deep, int $x): array {
    // Реализуйте функцию, которая случайным образом создает массив из X элементов,
    // состоящий из случайных целых чисел и из массивов целых чисел.
    // Глубина массива - $deep.
    // X должно быть больше 5 и меньше 10
    // Числа должны находиться в диапозоне от 10 до 10000
    $deep--;
    for ($i = 0; $i < $x; $i++) {
        if ($deep > 0) {
            $array[] = [];
            $array[$i] = createDeepArrayOfNumbers($array[$i], $deep, $x);
        }
        else {
            $array[] = random_int(10, 10000);
        }
    }
    return $array;
}

function calculateSum(array $deepArrayOfNumbers): int {
    // Напишите функцию которая вычисляет сумму чисел всех элементов и подэлементов структуры,
    // создаваемой функцией createDeepArrayOfNumbers.
    $sum=0;
    foreach ($deepArrayOfNumbers as $i){
        if (is_integer($i)){
            $sum +=$i;
        }
        else{
            $sum += calculateSum($i);
        }
    }
    return $sum;
}